#!/bin/sh

set -e

ARCH=$1

if [ -z "$ARCH" ]; then
  echo "Please specify the docker build arch"
  exit 0
fi

set -xe

curdir=`pwd`
dir=`mktemp -d -t XXXX.cpio.dir`
docker build . --no-cache -f "Dockerfile.$ARCH" -t pv-installer:$ARCH
sh -c "cd $dir; pvr init; fakeroot pvr app add --source=local --from=pv-installer:$ARCH test"
cd $dir; cp -f test/root.squashfs /tmp/;  unsquashfs test/root.squashfs
fakeroot sh -c "chmod 644 $dir/squashfs-root; cd $dir/squashfs-root; ls -la; find . -print0 | cpio --null -ov --format=newc | gzip -9 > $curdir/pv-installer.img;"
rm -rf $dir/
