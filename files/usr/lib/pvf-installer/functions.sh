#!/bin/sh

pcrstart() {
	tpm2_pcrreset 23

	# soft
	dd if=/dev/zero of=$spcr_file bs=1 count=32
}

pcrextendsha256() {
	extendhash=$1
	tpm2_pcrextend 23:sha256=$extendhash

	# soft
	echo -n $extendhash | xxd -r -p > $spcr_tmp1_file
	cat $spcr_file $spcr_tmp1_file | sha256sum -b | awk '{ print $1 }' | xxd -r -p > $spcr_tmp2_file
	cp -f $spcr_tmp2_file $spcr_file
}


pcrextendfile() {
	extendfile=$1
	hash=`sha256sum -b $extendfile | awk '{ print $1 }'`
	tpm2_pcrextend 23:sha256=$hash

	# soft
	sha256sum -b $extendfile | awk '{ print $1 }' | xxd -r -p > $spcr_tmp1_file
	cat $spcr_file $spcr_tmp1_file | sha256sum -b | awk '{ print $1 }' | xxd -r -p > $spcr_tmp2_file
	cp -f $spcr_tmp2_file $spcr_file
}

pcrdump() {
	dumpfile=$1
	tpm2_pcrread sha256:23 -o $dumpfile

	diff $dumpfile $spcr_file 2>&1 >/dev/null \
		|| echo "ERROR: SOFT PCR IS DIFFERENT - hard:`sha256sum -b $dumpfile`\n soft:`sha256sum -b $spcr_file`"
}

exit_invalid_dev() {
	dev=$1
	echo "Available Devices are:"
	for y in $dev
	do
			echo "/dev/$y"
	done
	exit 3
}

close_pvfactory() {
	umount /pvfactory || true
	sync
}

run_tpm_aca() {
	autojointoken_path=${3:-/pvfactory/.pvf/config/autojointoken.aca.json}
	ownerprn=`cat $autojointoken_path | jq -r '.owner'`
	token=`cat $autojointoken_path | jq -r '.token'`
	tokenid=`cat $autojointoken_path | jq -r '.id'`

	# Args: OWNERPRN AUTOJOINTOKEN TOKENID SOURCE_PH_CONFIG_FILE(optional) DEST_PH_CONFIG_FILE(optional)
	tpm-aca-factory $ownerprn $token $tokenid $1 $2
}

auto_eth0() {
	ifconfig eth0 up || true
	udhcpc || true
}

# compaq binary partition file is $1
# output folder keyvalue files
compaq2envd(){
	compaq=$1
	envd=$2

	cat $compaq | sed -E 's/\x00\x00.*//' > $compaq.next

	hasnext=1
	while [ "$hasnext" = 1 ]; do
		if cat $compaq.next | grep -qobUaP '\x00'; then
			cat $compaq.next | sed -En ':loop /.*\x00/{ s/(.*)\x00.*/\1/; p; q}; p' > $compaq.env
		else
			cp -f $compaq.next $compaq.env
			hasnext=0
		fi
		# strip key and val with xargs
		envkey=`cat $compaq.env | head -n1 | sed -e 's/=.*//'`
		if ! [ -z $envkey ]; then
			cat $compaq.env | sed -e 's/[^=]*=\(.*\)/\1/' > $envd/$envkey
		fi
		cat $compaq.next | sed -En '/\x00/{s/.*\x00//; p; :loop1 n; p; b loop1}' > $compaq.env
		cp -f $compaq.env $compaq.next
	done
}

